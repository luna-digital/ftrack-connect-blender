# ftrack connect blender #

Blender integration with ftrack.

### Documentation ###

To get started, obtain a copy of the source by cloning the public repository:

    git clone https://bitbucket.org/luna-digital/ftrack-connect-blender.git

Then you can build and install the package into your current Python site-packages folder:

    python setup.py build_plugin

The resulting plugin will then be available under the build folder. Copy or symlink the resulting plugin folder in your FTRACK_CONNECT_PLUGIN_PATH.

### Copyright and license ###

Copyright (c) 2020 Luna Digital, Ltd.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License. You may obtain a copy of the License in the LICENSE.txt file, or at:

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.

### Alpha 0.1.0 Changelog ###
* GUI updates
* Added support for local working files (so artists can save progress before committing to a full publish)
* Moved app icons to a dedicated host server (may move again in the future)
* Added a barebones framework for tracking objects by frack within Blender. More development needs to be done here. Currently, objects are aware of:
    * Whether or not it is an ftrack asset
    * Its associated asset ID
    * Its associated asset path
    * Its associated asset type
    * Its associated asset version
    * If the object isn't directly an asset, but the collection it's a part of is, a pointer to the actual ftrack asset (collection object)
    * Its component ID
* Publisher code is beginning to form, but nothing is functional yet. A temporary "Export to publish" button has been added to let you export a copy of your Blender file to your desktop (with relative paths intact) so you can publish through the ftrack Connect desktop app.
* If Blender's %APPDATA% (add-ons) directory is missing, it will be created by the integration to avoid erroring out. This should work on both Windows and macOS (Linux support to come soon).

### Screenshots ###

![Blender Connect Add-on](/assets/screenshots/0.1.0_gui.PNG)
![ftrack Connect Desktop App](/assets/screenshots/0.1.0_connect.PNG)
![ftrack Web Actions](/assets/screenshots/0.1.0_web.PNG)