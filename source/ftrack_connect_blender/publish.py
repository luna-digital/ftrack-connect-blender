# :coding: utf-8
# :copyright: Copyright (c) 2020 Luna Digital, Ltd.

import bpy

import tempfile
import os

def get_temporary_file_path(document_name):
    '''Return file path to *document name* in temporary directory.'''
    temporary_directory = tempfile.mkdtemp(prefix="ftrack_connect")
    file_path = os.path.join(
        temporary_directory, document_name
    )
    return file_path

def get_document_name():
    '''Return document name'''
    full_path = bpy.data.filepath
    document_name = os.path.split(full_path)[1]
    (name, extension) = os.path.splitext(document_name)
    return name

def export_blender_document():
    document_name = get_document_name()
    file_path = get_temporary_file_path(document_name)

    bpy.ops.wm.save_as_mainfile(
        filepath=file_path,
        relative_remap=False,
        copy=True
    )

    return file_path

def save_thumbnail_image():
    '''Save thumbnail as temporary file and return file path.'''
    pass

def publish(session, options):
    '''Publish a version based on *options*.'''
    document_path = export_blender_document()

    try:
        # Create new or get existing asset
        asset = session.ensure('Asset', {
            'context_id': options['parent'],
            'type_id': options['type'],
            'name': options['name']
        })

        version = session.create('AssetVersion', {
            'asset': asset,
            'task_id': options.get('task', None),
            'comment': options.get('description', '')
        })

        # Commit before adding components to ensure structures dependent on
        # committed ancestors work as expected.
        session.commit()

        try:
            thumbnail_path = save_thumbnail_image()
        except Exception:
            pass
        else:
            thumbnail_component = version.create_thumbnail(thumbnail_path)

        component = version.create_component(
            document_path,
            data=dict(name='blender-document', filetype='.blend'),
            location='auto'
        )

    except Exception:
        # On any exception, rollback and re-raise error
        session.rollback()
        raise

    return version['id']