# :coding: utf-8
# :copyright: Copyright (c) 2020 Luna Digital, Ltd.

import bpy

def ls(assetType=None):
    data = []

    for ob in bpy.data.objects:
        if (
            assetType == None or
            assetType.upper() == ob.type
        ):
            data.append(ob.name)
    
    return data