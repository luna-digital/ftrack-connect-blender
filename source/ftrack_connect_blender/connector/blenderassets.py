# :coding: utf-8
# :copyright: Copyright (c) 2020 Luna Digital, Ltd.

import bpy

from . import blendercmds as bc

import ftrack

from ftrack_connect.connector import (
    FTAssetType,
    FTComponent,
    HelpFunctions
)

blenderScene = bpy.context.scene

currentStartFrame = blenderScene.frame_start
currentEndFrame = blenderScene.frame_end

class GenericAsset(FTAssetType):

    def __init__(self):
        super(GenericAsset, self).__init__()
        self.appendAssetBool = False
        self.linkAssetBool = False

    def __saveTemporaryFile(self, path):
        bpy.ops.wm.save_as_mainfile(
            filepath=path,
            relative_remap=False,
            copy=True
        )
        
    def importAsset(self, iAObj=None):
        '''Import asset defined in *iAObj*'''

        pass

    def publishAsset(self, iAObj=None):
        '''Publish the asset defined by the provided *iAObj*.'''

        if hasattr(iAObj, 'customComponentName'):
            componentName = iAObj.customComponentName
        else:
            componentName = 'blend'
        
        publishedComponents = []

        temporaryPath = HelpFunctions.temporaryFile(suffix='.blend')
        publishedComponents.append(
            FTComponent(
                componentname=componentName,
                path=temporaryPath
            )
        )

        self.__saveTemporaryFile(temporaryPath)

        dependenciesVersion = []
        dependencies = bc.ls(type='ftrackAssetNode')


