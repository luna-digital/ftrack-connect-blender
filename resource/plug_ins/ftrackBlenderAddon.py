import bpy
import os

bl_info = {
    "name": "ftrack Connect for Blender",
    "blender": (2, 80, 0),
    "category": "Properties",
}

class FtrackSettings(bpy.types.PropertyGroup):
    has_working_file: bpy.props.BoolProperty()
    is_working_file: bpy.props.BoolProperty()

class FtrackAsset(bpy.types.PropertyGroup):
    is_asset: bpy.props.BoolProperty()
    asset_id: bpy.props.StringProperty()
    asset_path: bpy.props.StringProperty()
    asset_type: bpy.props.StringProperty()
    asset_version: bpy.props.StringProperty()
    collection_link: bpy.props.PointerProperty(name="Linked Asset", type=bpy.types.Collection)
    component_id: bpy.props.StringProperty()
    
class FTRACK_OT_import_asset(bpy.types.Operator):
    bl_idname = "ftrack.import_asset"
    bl_label = "Import Asset"
    
    def execute(self, context):
        return {'FINISHED'}
    
class FTRACK_OT_publish_asset(bpy.types.Operator):
    bl_idname = "ftrack.publish_asset"
    bl_label = "Publish"
    
    def execute(self, context):
        return {'FINISHED'}

class FTRACK_OT_save_working_file(bpy.types.Operator):
    bl_idname = "ftrack.save_working_file"
    bl_label = "Save working file"

    def execute(self, context):
        working_directory = bpy.path.abspath("//")
        basename = bpy.path.basename(context.blend_data.filepath).split(".")[0]

        bpy.ops.wm.save_as_mainfile(
            filepath = os.path.join(
                working_directory,
                basename + "_wf.blend"
            )
        )

        context.scene.ftrack_settings.is_working_file = True

        return {'FINISHED'}

class FTRACK_OT_open_working_file(bpy.types.Operator):
    bl_idname = "ftrack.open_working_file"
    bl_label = "Open working file"

    def execute(self, context):
        wf_path = context.blend_data.filepath.split(".")[0] + "_wf.blend"

        bpy.ops.wm.open_mainfile(
            filepath=wf_path
        )

        return {'FINISHED'}

class FTRACK_OT_export_to_publish(bpy.types.Operator):
    bl_idname = "ftrack.export_to_publish"
    bl_label = "Export to publish"

    def execute(self, context):
        ftrack = context.scene.ftrack_settings

        filename = bpy.path.basename(context.blend_data.filepath).split(".")[0]
        if (ftrack.is_working_file):
            filename = filename.split("_wf")[0]

        exportpath = os.path.join(
            os.path.expanduser('~'),
            "Desktop",
            filename + ".blend"
        )

        bpy.ops.wm.save_as_mainfile(
            filepath=exportpath,
            relative_remap=False,
            copy=True
        )

        return {'FINISHED'}

class FTRACK_PT_main_panel(bpy.types.Panel):
    bl_idname = "FTRACK_PT_main_panel"
    bl_label = "Blender Connect"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"
    
    def draw(self, context):
        col = self.layout.column()

class FTRACK_PT_import(bpy.types.Panel):
    bl_label = "Import"
    bl_parent_id = FTRACK_PT_main_panel.bl_idname
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    def draw(self, context):
        row = self.layout.row()
        row.operator(FTRACK_OT_import_asset.bl_idname)
        row.enabled = False

class FTRACK_PT_publish(bpy.types.Panel):
    bl_label = "Publish"
    bl_parent_id = FTRACK_PT_main_panel.bl_idname
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"

    def draw(self, context):
        row = self.layout.row()
        row.operator(FTRACK_OT_publish_asset.bl_idname)
        row.enabled = False

        row = self.layout.row()
        row.operator(FTRACK_OT_export_to_publish.bl_idname)

class FTRACK_PT_working_file(bpy.types.Panel):
    bl_label = "Working File"
    bl_parent_id = FTRACK_PT_main_panel.bl_idname
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'scene'

    def draw(self, context):
        ftrack = context.scene.ftrack_settings

        row = self.layout.row()
        row.operator(FTRACK_OT_save_working_file.bl_idname)
        row.enabled = not (ftrack.is_working_file or ftrack.has_working_file)

        row = self.layout.row()
        row.operator(FTRACK_OT_open_working_file.bl_idname)

        if ftrack.is_working_file:
            row.enabled = False
            notice = "Working file is active."
        elif ftrack.has_working_file and not ftrack.is_working_file:
            row.enabled = True
            notice = "A working file was found for this component."
        else:
            row.enabled = False
            notice = "No working file found."

        row = self.layout.row()
        row.label(text=notice)

def register():
    bpy.utils.register_class(FtrackSettings)
    bpy.utils.register_class(FtrackAsset)

    bpy.utils.register_class(FTRACK_OT_import_asset)
    bpy.utils.register_class(FTRACK_OT_publish_asset)
    bpy.utils.register_class(FTRACK_OT_save_working_file)
    bpy.utils.register_class(FTRACK_OT_open_working_file)
    bpy.utils.register_class(FTRACK_OT_export_to_publish)

    bpy.utils.register_class(FTRACK_PT_main_panel)
    bpy.utils.register_class(FTRACK_PT_import)
    bpy.utils.register_class(FTRACK_PT_publish)
    bpy.utils.register_class(FTRACK_PT_working_file)

    bpy.types.Scene.ftrack_settings = bpy.props.PointerProperty(type=FtrackSettings)

    bpy.types.Collection.ftrack = bpy.props.PointerProperty(type=FtrackAsset)
    bpy.types.Object.ftrack = bpy.props.PointerProperty(type=FtrackAsset)

def unregister():
    bpy.utils.unregister_class(FTRACK_PT_main_panel)
    bpy.utils.unregister_class(FTRACK_PT_import)
    bpy.utils.unregister_class(FTRACK_PT_publish)

    bpy.utils.unregister_class(FTRACK_OT_import_asset)
    bpy.utils.unregister_class(FTRACK_OT_save_working_file)
    bpy.utils.unregister_class(FTRACK_OT_open_working_file)
    bpy.utils.unregister_class(FTRACK_PT_working_file)
    bpy.utils.unregister_class(FTRACK_OT_publish_asset)
    bpy.utils.unregister_class(FTRACK_OT_export_to_publish)

    bpy.utils.unregister_class(FtrackAsset)
    bpy.utils.unregister_class(FtrackSettings)

    del bpy.types.Scene.ftrack_settings
    del bpy.types.Collection.ftrack
    del bpy.types.Object.ftrack

if __name__ == '__main__':
    register()