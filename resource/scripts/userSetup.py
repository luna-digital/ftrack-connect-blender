'''
Runs on Blender launch to make sure Blender Connect add-on is properly enabled.
'''

import bpy
import addon_utils

import os, sys, shutil
import getpass

import atexit

addon_name = 'ftrackBlenderAddon'
addon_file = addon_name + '.py'
addon_directory = bpy.utils.user_resource('SCRIPTS', "addons")

cwd = os.path.dirname(__file__)
ftrack_connect_blender_addon_path = os.path.abspath(os.path.join(cwd, '..',  'plug_ins'))

def configure_syspath():
    sys.path.append(os.environ["FTRACK_SYSPATH"])

def configure_addon():
    enabled, loaded = addon_utils.check(addon_name)

    if not loaded:
        shutil.copyfile(
            os.path.join(ftrack_connect_blender_addon_path, addon_file),
            os.path.join(addon_directory, addon_file)
        )

    if not enabled:
        addon_utils.enable(addon_name)

def has_working_file():
    if bpy.context.blend_data.filepath == '':
        return False

    working_directory = bpy.path.abspath("//")
    basename = bpy.path.basename(bpy.context.blend_data.filepath).split(".")[0]

    for filename in os.listdir(working_directory):
        if filename.endswith("_wf.blend") and filename.split("_wf")[0] == basename:
            return True

    return False

def register():
    configure_syspath()
    configure_addon()

    if has_working_file():
        bpy.context.scene.ftrack_settings.has_working_file = True

def unregister():
    os.remove(
        os.path.join(addon_directory, addon_file)
    )

if __name__ == '__main__':
    register()
    atexit.register(unregister)